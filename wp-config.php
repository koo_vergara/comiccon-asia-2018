<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'comiccon_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-_x~WHQ8zjRSwkoldzfGwklfBE|sBu.CotYv%./WFDA>k$.V(gTf?y#)x>Y8!8+y');
define('SECURE_AUTH_KEY',  'i|&0FXE>>I4w4U]iJ8(l.X`VsdcbtU`4Wdu$QkYS<x{i+,k{}iqr?%rv`A<NE5q>');
define('LOGGED_IN_KEY',    'V@kLMu23I)k*OqM^!s;EVY1cqa3Um~kSK8#g$JhFw)]Rq,Br%3}^BXAnwT>UXMl:');
define('NONCE_KEY',        '^>)Olc&H<d2QO$f;CrDPmZ{cJ|M_l8(DkmU0e.Csc E~)H&N13Y^RkG{i<~x+UUF');
define('AUTH_SALT',        'x;flc[2?,(q~0UCGjvLB:@b^TJDz{rUF5H.r!=COD)sscT[KA49S-s mnZtD#vuY');
define('SECURE_AUTH_SALT', '~vwv7dl6~y!h;z`@ElV_m511$1Nfdv>D^mt0:SpFTjVz|sO!v6*#TdYq7YQ}y(yl');
define('LOGGED_IN_SALT',   'YiDS^a%U-Irf83rhaX}#^_B0O*/Np)aN|+[QMjy;J4ua%3&T59~QfV15aiL(r];n');
define('NONCE_SALT',       '@kEE:Y4N-OyNVMI&xGg)U5:3D%,%;B1K>=lghev:Gq2?li%d)j;v3/JB3(^Q|3$!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
