<?php 
/*
Plugin Name: ComicCon Asia 2018
Plugin URI: http://comiccon.asia
Description: Plugin to create post type without modifiying the theme's function
Version: 1.0
Author URI: koo.vergara@gmail.com
*/

add_action('init', 'create_custom_posttype');

function create_custom_posttype()
{
	$postTypes = [
		'quests' => [
			's' => 'Quest',
			'p' => 'Quests',
			'c' => [[
				'singular' => 'Category',
				'plural' => 'Categories'
			]]
		]
	];

	$pos = 5;

	foreach($postTypes as $s => $v){
		if(!$v['supports']){
			$supports = ['title', 'editor', 'comments', 'thumbnail', 'page-attributes'];
		}
		else {
			$supports = $v['supports'];
		}

		$labels = [
			'name'					=> _x($v['p'],'post type general name'),
			'singular_name'			=> _x($v['s'],'post type singular name'),
			'add_new'				=> _x('Add New',$v['s']),
			'add_new_item'			=> __('Add New '.$v['s']),
			'edit_item'				=> __('Edit '.$v['s']),
			'new_item'				=> __('New '.$v['s']),
			'view_item'				=> __('View '.$v['s']),
			'search_items'			=> __('Search '.$v['p']),
			'not_found'				=> __('Nothing found'),
			'not_found_in_trash'	=> __('Nothing found in Trash')
		];
		$args = [
			'labels'		=> $labels,
			'public'		=> true,
			'hierarchical'	=> true,
			'menu_position'	=> $pos,
			'has_archive'	=> $s,
			'supports'		=> $supports,
			'rewrite'		=> ['slug' => $s],
		];

		if(isset($v['show_in_menu'])){
			$args['show_in_menu'] = $v['show_in_menu'];
		}

		register_post_type($s, $args);

		$pos++;
		
		if(count($v['c']) > 0){
			foreach($v['c'] as $taxonomy){
				$cplural = $taxonomy['plural'];
				$csingular = $taxonomy['singular'];
				$clabels = [
					'name'              => _x($cplural, 'taxonomy general name'),
					'singular_name'     => _x($csingular, 'taxonomy singular name'),
					'search_items'      => __('Search '.$csingular),
					'all_items'         => __('All '.$cplural),
					'parent_item'       => __('Parent '.$csingular),
					'parent_item_colon' => __('Parent '.$csingular.':'),
					'edit_item'         => __('Edit '.$csingular),
					'update_item'       => __('Update '.$csingular),
					'add_new_item'      => __('Add New '.$csingular),
					'new_item_name'     => __('New '.$csingular.' Name'),
					'menu_name'         => __($csingular),
				];
				
				$cargs = [
					'hierarchical'      => true,
					'labels'            => $clabels,
					'show_ui'           => true,
					'show_admin_column' => true,
					'query_var'         => true,
					'rewrite'           => ['slug' => $s.'_'.strtolower($csingular)],
				];

				register_taxonomy($s.'_'.strtolower($csingular), [$s], $cargs);
			}
		}
	}
}