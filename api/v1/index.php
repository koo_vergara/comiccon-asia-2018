<?php 
define('ROOT',				__DIR__);
define('DS',				DIRECTORY_SEPARATOR);
define('WP_USE_THEMES',		false);
define('COOKIE_DOMAIN',		false);
define('DISABLE_WP_CRON',	true);
define('FB_APP_ID', 		'1925561454126279');
define('FB_APP_SECRET', 	'94bbac362f58cdf3bea8f11bf970569a');
define('FB_GRAPH_VERSION',	'v2.11');
define('CCA_SALT',			'comicconasia2018');

require_once("../../wp-load.php");
require '../../vendor/autoload.php';

function paths()
{
	$uri = explode("?", $_SERVER['REQUEST_URI']);

	return array_filter(explode("/", str_replace(dirname($_SERVER['PHP_SELF']), "", $uri[0])));
}

$paths = paths();
$paths = array_values($paths);
$paths = array_map('ucfirst', $paths);

spl_autoload_register(function($class){
	$file = str_replace("\\", "/", dirname(__FILE__).DS."{$class}.php");

	if(!file_exists($file)){
		return false;
	}
	
	require $file;
});

$app = new App\Http($paths);
$app->run();