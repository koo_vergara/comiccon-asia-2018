<?php 
namespace App;
use App\Request;

class Http
{
	protected $vars = [];
	private $error = false;
	protected $headers = ['Access-Control-Allow-Origin: *', 'Content-Type: application/json; charset=UTF-8'];

	public function __set($index, $value)
	{
		$this->vars[$index] = $value;
	}

	public function __get($index)
	{
		return $this->vars[$index];
	}
	
	public function url($uri = "")
	{
		return $this->url.'/'.$uri;
	}

	public function __construct($paths)
	{
		$controller = $paths[0];

		unset($paths[0]);

		$paths = array_values($paths);
		$paths = array_map('ucfirst', $paths);
		$exec = "App\\$controller\Controller";
		$protocol = ($_SERVER['SERVER_PORT']) == 80 ? 'http' : 'https';

		if(!$paths){
			$paths = ['Index'];
		}

		$this->paths = $paths;
		$this->request = new Request();
		$uri = dirname($_SERVER['PHP_SELF']);
		$this->url = "{$protocol}://{$_SERVER['SERVER_NAME']}{$uri}";

		if(!class_exists($exec)){
			$this->exec = new Controller($this);

			if($controller != ''){
				$this->error = true;
			}
		}
		else {
			if(is_numeric($paths[0])){
				$this->paths = ['Index',$paths[0]];
			}

			$this->exec = new $exec($this);
		}
	}

	public function setHeader($headers)
	{
		$this->headers = $headers;
	}

	public function renderHeader()
	{
		foreach($this->headers as $header){
			header($header);
		}
	}

	public function run()
	{
		$this->renderHeader();

		if(method_exists($this->exec, 'play') && !$this->error){
			echo json_encode($this->exec->play());
		}
		else {
			echo json_encode($this->exec->getError());
		}
	}
}