<?php 
namespace App\Map;
use App\BaseController;
use App\Request;

class Controller extends BaseController
{
	public function getIndex()
	{
		$post = get_page_by_path('event-map');

		return [
			'status' => true,
			'code' => 200,
			'message' => '',
			'result' => [
				'title' => $post->post_title,
				'content' => strip_tags($post->post_content)
			]
		];
	}
}