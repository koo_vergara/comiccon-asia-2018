<?php 
namespace App\Quest;
use App\BaseController;
use App\Request;

class Controller extends BaseController
{
	public function getIndex()
	{
		$post = get_page_by_path('quests');
		$quests = query_posts([
			'post_type' 	=> 'quests',
			'post_parent' 	=> 0,
			'orderby'		=> 'id',
			'order'			=> 'asc'
		]);
		$zones = [];

		foreach($quests as $quest){
			$categories = [];

			$children = query_posts([
				'post_type'		=> 'quests',
				'post_parent' 	=> $quest->ID,
				'orderby'		=> 'id',
				'order'			=> 'asc'
			]);

			foreach($children as $child){
				$category = get_the_terms($child->ID, 'quests_category');
				
				foreach($category as $cat){
					$count = new \WP_Query([
						'post_type' 	=> 'quests',
						'post_parent' 	=> $quest->ID,
						'tax_query' 	=> [[
							'taxonomy' 	=> 'quests_category',
							'terms' 	=> $cat->term_id
						]]
					]);
					$categories[$cat->slug] = [
						'count' => $count->found_posts,
						'label' => $cat->name
					];
				}
			}

			$zones[] = [
				'title' 		=> $quest->post_title,
				'content'		=> strip_tags($quest->post_content),
				'url'			=> $this->app->url('quest/zone/'.$quest->ID),
				'bg_color' 		=> get_field('background_color', $quest->ID),
				'categories'	=> $categories,
			];
		}

		return [
			'status' => true,
			'code' => 200,
			'message' => '',
			'result' => [
				'title' 	=> $post->post_title,
				'content' 	=> strip_tags($post->post_content),
				'zones' 	=> $zones
			]
		];
	}

	public function getZone()
	{
		$id = $this->app->paths[1] ? $this->app->paths[1] : 0;
		$post = get_post($id);
		$category = get_terms('quests_category');
		$categories = [];

		foreach($category as $cat){
			$posts = query_posts([
				'post_type' 	=> 'quests',
				'post_parent' 	=> $id,
				'tax_query' 	=> [[
					'taxonomy' 	=> 'quests_category',
					'terms' 	=> $cat->term_id
				]],
				'orderby'		=> 'id',
				'order'			=> 'asc'
			]);
			$quests = [];

			foreach($posts as $post){
				$quests[] = [
					'title' 	=> $post->post_title,
					'url'		=> $this->app->url('quest/quest/'.$post->ID),
					'status'	=> ''
				];			
			}

			$categories[$cat->slug] = [
				'label' 	=> $cat->name,
				'quests' 	=> $quests
			];
		}

		return [
			'status' => true,
			'code' => 200,
			'message' => '',
			'result' => [
				'title' 	=> $post->post_title,
				'quests'	=> $categories
			]
		];
	}

	public function getQuest()
	{
		$id = $this->app->paths[1] ? $this->app->paths[1] : 0;
		$post = get_post($id);

		return [
			'status' => true,
			'code' => 200,
			'message' => '',
			'result' => [
				'title' 	=> $post->post_title,
				'content'	=> strip_tags($post->post_content)
			]
		];
	}
}