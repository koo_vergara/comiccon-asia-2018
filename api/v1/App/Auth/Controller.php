<?php 
namespace App\Auth;
use App\BaseController;
use App\Request;
use \Facebook\Facebook;
use \Facebook\Exceptions\FacebookResponseException;
use \Facebook\Exceptions\FacebookSDKException;

class Controller extends BaseController
{
	public function getRegister()
	{
		return [
			'status' 	=> true,
			'code' 		=> 200,
			'message'	=> '',
			'actions' 	=> [
				'method' 	=> 'post',
				'url'		=> $this->app->url('auth/register')
			],
			'result' 	=> [[
				'required'		=> true,
				'type'			=> 'text',
				'name'			=> 'username',
				'placeholder' 	=> 'Username',
				'label' 		=> 'Username'
			],[
				'required'		=> true,
				'type'			=> 'text',
				'name'			=> 'first_name',
				'placeholder' 	=> 'First Name',
				'label' 		=> 'First Name'
			],[
				'required'		=> true,
				'type'			=> 'text',
				'name'			=> 'last_name',
				'placeholder' 	=> 'Last Name',
				'label' 		=> 'Last Name'
			],[
				'required'		=> true,
				'type'			=> 'email',
				'name'			=> 'email',
				'placeholder' 	=> 'Email Address',
				'label' 		=> 'Email Address'
			],[
				'required'		=> true,
				'type'			=> 'password',
				'name'			=> 'password',
				'placeholder' 	=> 'Password',
				'label' 		=> 'Password'
			],[
				'required'		=> true,
				'type'			=> 'password',
				'name'			=> 'confirm_password',
				'placeholder' 	=> 'Confirm Password',
				'label' 		=> 'Confirm Password'
			]]
		];
	}

	public function postRegister()
	{
		if($user = $this->user()){
			return [
				'status' 	=> false,
				'code'		=> 400,
				'message'	=> 'Please logout first to register a new account.'
			];
		}

		$first_name			= $this->request->post('first_name');
		$last_name			= $this->request->post('last_name');
		$username 			= $this->request->post('username');
		$email 				= $this->request->post('email');	
		$password 			= $this->request->post('password');
		$confirm_password 	= $this->request->post('confirm_password');

		if(username_exists($username)){
			return [
				'status' 	=> false,
				'code'		=> 400,
				'message'	=> 'Username already exists. Please try another.'
			];
		}

		if($email == ''){
			return [
				'status' 	=> false,
				'code'		=> 400,
				'message'	=> 'Please enter your email.'
			];
		}

		if(!is_email($email)){
			return [
				'status' 	=> false,
				'code'		=> 400,
				'message'	=> 'Please enter a valid email.'
			];
		}

		if(email_exists($email)){
			return [
				'status' 	=> false,
				'code'		=> 400,
				'message'	=> 'Email address already exists. Please try another.'
			];
		}

		if($password != $confirm_password){
			return [
				'status' 	=> false,
				'code'		=> 400,
				'message'	=> 'Password does not match. Please confirm your password.'
			];
		}

		if($first_name == ''){
			return [
				'status' 	=> false,
				'code'		=> 400,
				'message'	=> 'Please enter your first name.'
			];	
		}

		if($last_name == ''){
			return [
				'status' 	=> false,
				'code'		=> 400,
				'message'	=> 'Please enter your last name.'
			];	
		}

		$user_id = wp_create_user($username, $password, $email);

		if(is_int($user_id)){
			$wp_user_object = new \WP_User($user_id);
			$wp_user_object->set_role('subscriber');

			update_user_meta($user_id, 'first_name', $first_name);
			update_user_meta($user_id, 'last_name', $last_name);

			return [
				'status'	=> true,
				'code'		=> 200,
				'message'	=> 'Your account has been successfully created. You may now login.',
				'result'	=> $this->getUserByID($user_id)
			];
		}

		return [
			'status' 	=> false,
			'code'		=> 400,
			'message'	=> 'Something went wrong.'
		];
	}

	public function getLogin()
	{
		return [
			'status'	=> true,
			'code'		=> 200,
			'message'	=> '',
			'actions'	=> [
				'login' 	=> [
					'method' 	=> 'post',
					'url'		=> $this->app->url('auth/login')
				],
				'register' 	=> [
					'method' 	=> 'get',
					'url'		=> $this->app->url('auth/register')
				],
				'facebook'	=> [
					'method'	=> 'post',
					'url'		=> ''
				],
				'google'	=> [
					'method'	=> 'post',
					'url'		=> ''
				]
			],
			'result'	=> [
				[
					'required'		=> true,
					'type'			=> 'text',
					'name'			=> 'email',
					'placeholder' 	=> 'Email',
					'label' 		=> 'Email'
				],
				[
					'required'		=> true,
					'type'			=> 'password',
					'name'			=> 'password',
					'placeholder' 	=> 'Password',
					'label' 		=> 'Password'
				],
			]
		];
	}

	public function postLogin()
	{
		if($user = $this->user()){
			$token = $this->request->header('X-Auth-Token');
			$expire_at = date('Y-m-d H:i:s',strtotime("+1 month"));

			update_user_meta($user['ID'], 'user_token_expire_at', $expire_at);

			return [
				'status'	=> true,
				'code'		=> 200,
				'message'	=> 'Successfully logged in.',
				'result'	=> [
					'token'		=> $token,
					'expire_at'	=> $expire_at,
					'profile'	=> $this->user()
				]
			];
		}

		$user = get_user_by('email', $this->request->post('email'));
		
		if(!$user){
			return [
				'status'	=> false,
				'code'		=> 400,
				'message'	=> 'Invalid email.',
				'tag'		=> 0
			];
		}
		elseif(!wp_check_password($this->request->post('password'), $user->data->user_pass, $user->ID)){
			return [
				'status'	=> false,
				'code'		=> 400,
				'message'	=> 'Invalid password.',
				'tag'		=> 1
			];
		}

		$token = $this->generate_token($user->ID);

		update_user_meta($user->ID, 'user_token', $token['token']);
		update_user_meta($user->ID, 'user_token_expire_at', $token['expire_at']);

		return [
			'status'	=> true,
			'code'		=> 200,
			'message'	=> 'Successfully logged in.',
			'result'	=> [
				'token'		=> $token['token'],
				'expire_at'	=> $token['expire_at'],
				'profile'	=> $this->user()
			],
		];
	}

	public function postFacebook()
	{
		if($user = $this->user()){
			$token = $this->request->header('X-Auth-Token');
			$expire_at = date('Y-m-d H:i:s',strtotime("+1 month"));

			update_user_meta($user['ID'], 'user_token_expire_at', $expire_at);

			return [
				'status'	=> true,
				'code'		=> 200,
				'message'	=> 'Successfully logged in.',
				'result'	=> [
					'token'		=> $token,
					'expire_at'	=> $expire_at,
					'profile'	=> $this->user()
				]
			];
		}

		$accessToken = $this->app->paths[1] ? $this->app->paths[1] : 0;
		$fb = $this->fb_connect();
		$oAuth2Client = $fb->getOAuth2Client();
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
		$tokenMetadata->validateAppId(FB_APP_ID);
		$tokenMetadata->validateExpiration();

		if(!$accessToken->isLongLived()){
			try {
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
			}
			catch(FacebookSDKException $e){
				return [
					'status' => false,
					'code' => 400,
					'message' => 'Error getting long-lived access token: '.$e->getMessage()
				];
			}
		}

		$profile = $this->fb_profile($accessToken);

		if(empty($profile['email'])){
			return [
				'status' 	=> false,
				'code' 		=> 300,
				'message' 	=> 'Please allow this app...'
			];
		}

		if(email_exists($profile['email'])){
			$user 		= get_user_by('email', $profile['email']);
			$expire_at 	= date('Y-m-d H:i:s',strtotime("+1 month"));
			$avatar 	= get_user_meta($user->ID, 'user_avatar', true);
			$fb_avatar 	= get_user_meta($user->ID, 'user_fb_avatar', true);

			if(empty($avatar) && empty($fb_avatar)){
				update_user_meta($user->ID, 'user_fb_avatar', $profile['picture']['data']['url']);
			}

			update_user_meta($user->ID, 'user_token_expire_at', $expire_at);

			return [
				'status'	=> true,
				'code'		=> 200,
				'message'	=> 'Successfully logged in.',
				'result'	=> [
					'token'		=> get_user_meta($user->ID, 'user_token', true),
					'expire_at'	=> $expire_at,
					'profile'	=> $this->getUserByID($user->ID)
				],
			];
		}

		$password = wp_generate_password(8, false);
		$user_id = wp_create_user($profile['email'], $password, $profile['email']);

		if(!is_int($user_id)){
			return [
				'status' 	=> false,
				'code'		=> 400,
				'message'	=> 'Something went wrong.'
			];
		}

		$wp_user_object = new \WP_User($user_id);
		$wp_user_object->set_role('subscriber');

		update_user_meta($user_id, 'first_name', $profile['first_name']);
		update_user_meta($user_id, 'last_name', $profile['last_name']);
		update_user_meta($user_id, 'user_fb_avatar', $profile['picture']['data']['url']);

		$token = $this->generate_token($user_id);

		update_user_meta($user_id, 'user_token', $token['token']);
		update_user_meta($user_id, 'user_token_expire_at', $token['expire_at']);

		return [
			'status'	=> true,
			'code'		=> 200,
			'message'	=> 'Successfully logged in.',
			'result'	=> [
				'token'		=> $token['token'],
				'expire_at'	=> $token['expire_at'],
				'profile'	=> $this->getUserByID($user_id)
			],
		];
	}

	public function fb_connect()
	{
		$fb = new Facebook([
			'app_id' 				=> FB_APP_ID,
			'app_secret' 			=> FB_APP_SECRET,
			'default_graph_version' => FB_GRAPH_VERSION
		]);

		return $fb;
	}

	public function fb_profile($accessToken)
	{
		$fb = $this->fb_connect();

		try {
  			$response = $fb->get('/me?fields=id,first_name,last_name,email,picture', $accessToken);
		}
		catch(FacebookResponseException $e){
  			return [
				'status' => false,
				'code' => 400,
				'message' => 'Graph returned an error: '.$e->getMessage()
			];
		}
		catch(FacebookSDKException $e){
  			return [
				'status' => false,
				'code' => 400,
				'message' => 'Facebook SDK returned an error: '.$e->getMessage()
			];
		}

		$user = $response->getGraphNode();

		return [
			'id' => $user['id'],
			'first_name' => $user['first_name'],
			'last_name' => $user['last_name'],
			'email' => $user['email'],
			'picture' => $user['picture']
		];
	}

	public function generate_token($id)
	{
		$string = microtime().'-'.$id.'-'.CCA_SALT;
		$token = (empty(get_user_meta($id, 'user_token', true))) ? base64_encode($string) : get_user_meta($id, 'user_token', true);
		$expire_at = date('Y-m-d H:i:s',strtotime("+1 month"));

		return [
			'token' => $token,
			'expire_at' => $expire_at
		];
	}
}