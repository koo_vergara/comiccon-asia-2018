<?php 
namespace App;

class Request
{
	public function clean($val)
	{
		if(is_array($val))
		{ 
			foreach($val as $k => $v){
				$pp[$k] = htmlspecialchars(addslashes(trim($v)));
			}

			$p = $pp;
		}
		else {
			$p = htmlspecialchars(addslashes(trim($val)));
		}

		return $p;
	}

	public function post($fieldname = "", $value = "")
	{
		if(!isset($_POST[$fieldname])){
			return false;
		}

		$valued = $this->clean($_POST[$fieldname]);
		$post = ($value == '') ? $valued : $value;
		
		return $post;
	}

	public function get($fieldname = "", $value = "")
	{
		$get = isset($_GET[$fieldname]) ? ( trim($_GET[$fieldname]) != '' ) ? $_GET[$fieldname] : ($value!='' ? $value : false) : ($value!='' ? $value : false);

		if(!$get){
			return false;
		}

		$valued = $this->clean($get); 
		
		return $get;
	}

	public function header($fieldname = "", $value = "")
	{
		$headers = apache_request_headers();

		if(!isset($headers[$fieldname])){
			return false;
		}

		$valued = $this->clean($headers[$fieldname]); 
		$get = ($value == '') ? $valued : $value;
		
		$this->$fieldname = $get;
		
		return $get;
	}
	
	public function isEmail($email)
	{
		if(preg_match("/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/i", $email)){
			return true;
		}

		return false;
	}

	public function hasSymbol($data)
	{
		if(preg_match("~^[\-a-z 0-9]*\z~i", $data)){
			return false;
		}

		return true;
	}
	
	public function hasSpace($data){
		if(preg_match("/^[a-zA-Z0-9_.-]+$/i", $data)){
			return false;
		}

		return true;
	}

	public function parse(array &$post)
	{
		$input = file_get_contents('php://input');

		preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
		
		$boundary = $matches[1];
		$blocks = preg_split("/-+$boundary/", $input);

		array_pop($blocks);

		foreach($blocks as $id => $block){
			if(empty($block))
				continue;
			
			if(strpos($block, 'application/octet-stream') !== FALSE){
				preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
			}
			else {
				preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
			}

			if($matches[1] != ""){
				$post[$matches[1]] = $matches[2];
			}
		}
	}

	public function isParsable()
	{
	  	$input = file_get_contents('php://input');
	  	$blocks = preg_split("/-+$boundary/", $input);

	  	if($blocks){
		  	if(count($blocks) > 1){
		  		return true;
		  	}
		  	else {
		  		parse_str($blocks[0], $checkparsed); // not sure if I have to cache this too.

		  		return false;
		  	}
	  	}
	  	else {
	  		return false;
	  	}
	}
}