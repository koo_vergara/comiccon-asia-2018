<?php 
namespace App;

abstract class BaseController
{
	public function __construct($app)
	{
		$this->app = $app;
		$this->request = $app->request;
		$this->setHeaderJson();

	    ob_start();

	    var_dump($_REQUEST);
	    var_dump($this->request->header('X-Auth-Token'));
	    var_dump(apache_request_headers());
	    
    	$_content = ob_get_contents();
	    ob_end_clean();
	    file_put_contents(ROOT . DS . 'logs.txt', $_content);
	}

	private function setHeaderJson()
	{
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
	}

	private function setHeaderImage($mime, $length)
	{
        header('Content-Type: ' . $mime);
        header('Content-Length: ' . $length);
	}

	public function play()
	{
		$method = strtolower($_SERVER['REQUEST_METHOD']) . $this->app->paths[0];

		if(is_callable([$this, $method]) === false){
			return $this->getError();
		}
		else {
			return $this->$method();
		}
	}

	public function getError()
	{
		return  [
			'status'	=> false,
			'message'	=> 'API does not exists.'
		];
	}

	public function getIndex()
	{
		return  [
			'status'	=> true,
			'message'	=> 'Default Controller Method.'
		];
	}

	public function user()
	{
		$user_id = reset(get_users(['fields'=>'id','meta_key' => 'user_token', 'meta_value' => $this->request->header('X-Auth-Token')]));

		if(!$user_id){
			return false;
		}

		return $this->getUserByID($user_id);
	}

	public function getUserByID($user_id)
	{
		$user = get_userdata($user_id);
		
		return [
			'ID' 				=> $user_id,
			'username'			=> $user->user_login,
			'email'				=> $user->user_email,
			'first_name'		=> $user->first_name,
			'last_name'			=> $user->last_name,
			'avatar'			=> get_user_meta($user_id, 'user_avatar', true),
			'rfid'				=> get_user_meta($user_id, 'user_rfid', true),
			'date_registered'	=> $user->user_registered,
		];
	}

	public function login()
	{
		return [
			'status' 	=> false,
			'message'	=> 'Please login to view news.',
			'api'		=> $this->app->url('auth/login'),
		];
	}

	public function isTokenExpired($user_id)
	{
		$expiration = get_user_meta($user_id, 'user_token_expire_at', true);
		$now = date('Y-m-d H:i:s');

		if(empty($expiration)){
			return true;
		}
		
		if(strtotime($expiration) >= strtotime($now)){
			return false;
		}

		return true;
	}

	public function get_acf_key($field_name)
	{
	    global $wpdb;

	    return $wpdb->get_var("
	        SELECT post_name
	        FROM $wpdb->posts
	        WHERE post_type='acf-field' AND post_excerpt='$field_name';
	    ");
	}
}