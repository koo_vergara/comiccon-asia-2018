<?php 
namespace App\User;
use App\BaseController;
use App\Request;

class Controller extends BaseController
{
	public function getProfile()
	{
		$user = $this->user();

		return [
			'status' 	=> true,
			'code' 		=> 200,
			'message' 	=> '',
			'actions' 	=> [
				'method' 	=> 'post',
				'url' 		=> $this->app->url('user/profile')
			],
			'result' 	=> [[
				'required'		=> true,
				'type'			=> 'hidden',
				'name'			=> 'id',
				'value'			=> $user['ID']
			],[
				'required'		=> true,
				'type'			=> 'text',
				'name'			=> 'first_name',
				'placeholder' 	=> 'First Name',
				'label' 		=> 'First Name',
				'value'			=> $user['first_name']
			],[
				'required'		=> true,
				'type'			=> 'text',
				'name'			=> 'last_name',
				'placeholder' 	=> 'Last Name',
				'label' 		=> 'Last Name',
				'value'			=> $user['last_name']
			],[
				'required'		=> true,
				'type'			=> 'email',
				'name'			=> 'email',
				'placeholder' 	=> 'Email Address',
				'label' 		=> 'Email Address',
				'value'			=> $user['email']
			],[
				'required'		=> true,
				'type'			=> 'password',
				'name'			=> 'password',
				'placeholder' 	=> 'Password',
				'label' 		=> 'Password'
			],[
				'required'		=> true,
				'type'			=> 'password',
				'name'			=> 'confirm_password',
				'placeholder' 	=> 'Confirm Password',
				'label' 		=> 'Confirm Password'
			],[
				'required'		=> true,
				'type'			=> 'text',
				'name'			=> 'avatar',
				'value'			=> $user['avatar']
			],[
				'required'		=> true,
				'type'			=> 'text',
				'name'			=> 'rfid',
				'placeholder' 	=> 'RFID',
				'label' 		=> 'RFID',
				'value'			=> $user['rfid']
			]]
		];
	}

	public function postProfile()
	{
		$id 				= $this->request->post('id');
		$first_name			= $this->request->post('first_name');
		$last_name			= $this->request->post('last_name');
		$email 				= $this->request->post('email');	
		$password 			= $this->request->post('password');
		$confirm_password 	= $this->request->post('confirm_password');
		$avatar				= $this->request->post('avatar');
		$rfid 				= $this->request->post('rfid');

		if($this->user()){
			$userdata = [
				'ID' => $id
			];

			if(empty($first_name) || empty($last_name) || empty($email)){
				return [
					'status'	=> false,
					'code'		=> 400,
					'message'	=> 'Please fill in required fields.'
				];
			}

			if(!empty($first_name)){
				$userdata['first_name'] = $first_name;
			}

			
		}
	}
}